import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CmsIntropComponent } from 'src/components/cms-introp/cms-introp.component';
import { DashboardComponent } from 'src/components/dashboard/dashboard.component';
import { HeaderComponent } from 'src/components/header/header.component';
import { HomePageComponent } from 'src/components/home-page/home-page.component';
import { LoginComponent } from 'src/components/login/login.component';
import { ProvidersComponent } from 'src/components/providers/providers.component';
import { WelComeComponent } from 'src/components/wel-come/wel-come.component';
import { MyInsurersComponent } from '../components/my-insurers/my-insurers.component';

const routes: Routes = [
  {
    path:"login",
    component:LoginComponent
  },
  {
    path:"setting",
    component:CmsIntropComponent
  },
  {
    path:"",
    component:WelComeComponent
  },
  
  {
    path:"insurers",
    component: MyInsurersComponent 
  },

  {
    path:"fhir-app",
    component: HeaderComponent 
  },

  {
    path:"header",
    component: HeaderComponent 
    
  },

  {
    path:"header",
    component: HeaderComponent
  },
  {
    path:"providers",
    component: ProvidersComponent
  },
  {
    path:"dashboard",
    component: DashboardComponent
  }




  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
