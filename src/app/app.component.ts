import { Component } from '@angular/core';
import { fhirclient } from 'fhirclient/lib/types';

//  (window as any).FHIR = FHIR;
// declare var FHIR: any
// (window as any).FHIR = FHIR;
declare var FHIR: any
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Health-Chain';
  ngOnInit() {
  }
}