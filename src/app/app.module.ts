import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HeaderComponent } from 'src/components/header/header.component';
import { BaseService } from 'src/services/base.service';
import { HomePageComponent } from 'src/components/home-page/home-page.component';
import { DashboardComponent } from 'src/components/dashboard/dashboard.component';
import { CommonModule } from '@angular/common';
import { ProvidersComponent } from 'src/components/providers/providers.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyInsurersComponent } from 'src/components/my-insurers/my-insurers.component';
import { VitalSignComponent } from 'src/components/vital-sign/vital-sign.component';
import { MedicalHistoryComponent } from 'src/components/medical-history/medical-history.component';
import { PatientProfileComponent } from 'src/components/patient-profile/patient-profile.component';
import { LoginComponent } from 'src/components/login/login.component';
import { VisitHistoryComponent } from 'src/components/visit-history/visit-history.component';
import { WelComeComponent } from 'src/components/wel-come/wel-come.component';
import { RequestInterceptor } from 'src/core/interceptors/request.interceptor';
import { NgxPaginationModule} from 'ngx-pagination'; 
import { OnCreate } from 'src/directives/onCreate.directive';
import { CmsIntropComponent} from 'src/components/cms-introp/cms-introp.component';
import FHIR from "fhirclient";
import { LabresultComponent } from 'src/components/labresult/labresult.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent, 
    DashboardComponent, 
    MyInsurersComponent,
    ProvidersComponent, 
    VitalSignComponent,
    MedicalHistoryComponent,
    PatientProfileComponent,
    VisitHistoryComponent,
    WelComeComponent,
    CmsIntropComponent,
    OnCreate,
    LoginComponent,
    LabresultComponent,
  //  PaginationComponent
  ],
  imports: [
    BrowserModule,  
    AppRoutingModule,
    HttpClientModule, 
    NgxPaginationModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [BaseService, DatePipe,{
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptor,
    multi: true
  }, { provide: ErrorHandler} ], 
  exports:[DashboardComponent,HeaderComponent,OnCreate], 
  entryComponents:[HomePageComponent,DashboardComponent,MyInsurersComponent,ProvidersComponent, HeaderComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA] 
})
export class AppModule { } 
