import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CmsIntropComponent } from './cms-introp.component';

describe('CmsIntropComponent', () => {
  let component: CmsIntropComponent;
  let fixture: ComponentFixture<CmsIntropComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CmsIntropComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CmsIntropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
