import { Component, OnInit } from '@angular/core';
import { writeFile, writeFileSync } from 'fs';
import { FormGroup, FormControl, FormArray, NgForm } from '@angular/forms';
import { ApiService } from 'src/services/api.service';
import { environment } from 'src/environments/environment';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cms-introp',
  templateUrl: './cms-introp.component.html',
  styleUrls: ['./cms-introp.component.css']
})
export class CmsIntropComponent implements OnInit {

  //public myForm: FormGroup;

  ab:any;
  container:any={};
  provider:any={};
  selected:boolean=false;
  checked:any='checked';
  constructor(private router:Router, public common : ApiService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.common.apiCall(environment.json_url).subscribe((res:any)=>{
      this.container['providers'] =res;
     });
  }


  saveInteractive(name:string,client:string,client_secret:string, authority:string,redirect_uri:string,scope:string,endpoint:string,
    patient_id:string,bypasslogin:string,update:string) {
   //   alert(update);
      this.common.apiPost(name,client,client_secret, authority,redirect_uri,scope,endpoint,bypasslogin,patient_id,update).subscribe((res:any)=>{
        this.router.navigateByUrl('/');
      });
  }

  saveBatchTest(num1:string,num2:string,num3:string,num4:string) {
    //alert(num1+"-"+num2+"-"+num3+"-"+num4);
    // writeFileNEW();
  }

  getProvider(value:any){
    if(value!='Choose a health care provider'){
    let data:[] =this.container['providers'];
      this.provider =data.find((obj=>obj['name']==value))
      //this.provider=JSON.stringify(this.provider);
      //localStorage.setItem('provider',this.provider);
      this.selected=true;
    }
  }

  saveJSON(data:any){
  this.ab = {
            "name": data.name,
            "clinet_id": data.clinet_id,
            "athority": data.athority,
            "redirect_uri": data.redirect_uri,
            "scope": data.scope,
            "endpoint": data.endpoint,
            "bypasslogin": data.bypasslogin
          }
    }
    checkValue(event: any){
   console.log(event);
}

onCheckboxChange(e:any) {
  if (e.target.checked) {
    e.target.value='true';
  } else {
    e.target.value='false';
  }
}





}
