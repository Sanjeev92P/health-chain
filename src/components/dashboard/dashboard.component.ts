import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { BaseService } from 'src/services/base.service';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';
import { Router } from '@angular/router';
import FHIR, { client } from "fhirclient";

import { ProvidersComponent } from '../providers/providers.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild('dynamicPlaceHoloder',{read:ViewContainerRef}) entry:any;
  isLoadedScreen:boolean=true;
  container:any={}; 
  httpOptions: any;
  Observation:any={};
  phone:any;
  email:any;

  constructor(private apiService:ApiService, private router:Router,private componentFactoryResolver: ComponentFactoryResolver) {
    this.isLoadedScreen=true;
    var provider:any= localStorage.getItem('provider');
    provider=JSON.parse(provider);
   if(provider.name=='Cerner'){
      this.getCernerPatient();
    }else{
      this.getPatient();
    }
    this.container['isLoaded'] = true;
   }

  ngOnInit(): void {
    var provider:any= localStorage.getItem('provider');
        provider=JSON.parse(provider);

    if(provider.name=='Cerner'){
      this.getCernerPatient();
    }else{
      this.getPatient();
    }
  this.container['isLoaded'] = true;
  }
  goToDetails(){ 
    this.isLoadedScreen=false;
  }
  back(){
    this.isLoadedScreen=true;
  }



  getCernerPatient(){
    FHIR.oauth2.ready().then(client=>client.request('Patient/'+client.patient.id)).then(res=>{
      this.container = res; 
      localStorage.setItem('userdetail',JSON.stringify(res));
    })
  }

  getPatient(){
  

    this.apiService.getPatient().subscribe((res:any)=>{
      if(res){
          this.container = res;
          let data:[] =res.telecom;
          this.phone = data.find((obj=>obj['system']=='phone'));
          this.email =data.find((obj=>obj['system']=='email'));

          localStorage.setItem('userdetail',JSON.stringify(res));
        }
    },(err)=>{
   
    })


    

  this.apiService.Observation('').subscribe((res:any)=>{
    if(res){
        this.Observation = res;

      }
    
  },(err)=>{
 
});
  }
  goToClaims(type:string){
    this.isLoadedScreen = false;
    if(type=='doctors'){
    this.container['isClaims'] = false;
    this.container['isProvider'] = true;
    }else{
    this.container['isClaims'] = true;
    this.container['isProvider'] = false;
    }
  }
}
