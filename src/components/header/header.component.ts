import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { ViewCompileResult } from '@angular/compiler/src/view_compiler/view_compiler';
import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { HomePageComponent } from '../home-page/home-page.component'
import { MyInsurersComponent } from '../my-insurers/my-insurers.component';
import { ProvidersComponent } from '../providers/providers.component';
import { VitalSignComponent } from '../vital-sign/vital-sign.component';
import { MedicalHistoryComponent } from '../medical-history/medical-history.component';
import { PatientProfileComponent } from '../patient-profile/patient-profile.component';
import { VisitHistoryComponent } from '../visit-history/visit-history.component';
import { LabresultComponent } from '../labresult/labresult.component';
import * as Msal from "msal";
import FHIR, { client } from "fhirclient";
import { from } from 'rxjs';
import { ApiService } from 'src/services/api.service';
import $ from "jquery";

declare function signOut():any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('dynamicComponentPlaceHoloder',{read:ViewContainerRef}) entry:any;
  isDashboard:boolean=true;
  container:any={};
  msg:any='';
    
  provider:any={};
  title:any='';
  user:any={};
  public active = "home"
  
  constructor(private router:Router,
    private componentFactoryResolver: ComponentFactoryResolver,private apiService:ApiService,) { 
    var provider:any= localStorage.getItem('provider');
    provider=JSON.parse(provider);
   if(provider.name=='Cerner'){
      this.getCernerPatient();
    }else{
      this.getPatient();
    
    }
    this.container['isLoaded'] = true;
   
    }

  ngOnInit(): void {
    var today = new Date()
    var curHr = today.getHours()

    if (curHr < 12) {
    } else if (curHr < 18) {
     this.msg="Good Afternoon" ;

    } else {
      this.msg="Good Evening" ;
    }
  

  }
  ngAfterViewInit(){
    this.createComponent(DashboardComponent);
    this.isDashboard =true;
  }


  getCernerPatient(){
    FHIR.oauth2.ready().then(client=>client.request('Patient/'+client.patient.id)).then(res=>{
      this.container = res; 
      localStorage.setItem('userdetail',JSON.stringify(res));
    })
  }

  getPatient(){
  

    this.apiService.getPatient().subscribe((res:any)=>{
      if(res){
          this.container = res; 
          this.user=res;
          localStorage.setItem('userdetail',JSON.stringify(res));
        }
      
    },(err)=>{
    })
    
  }


  goToDashBoard(link:any){
    //this.router.navigate(['home']);
    this.title='';
    this.createComponent(DashboardComponent);
    this.isDashboard =true;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
    element.click();
  }

  goToProviders(link:any){
    this.title='Provider Details';
    this.createComponent(ProvidersComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
element.click();
  }

  insuranceInfo(link:any){
    this.title='Insurance Details';
    this.createComponent(MyInsurersComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
element.click();

  }
  goToHome(link:any){
    //this.router.navigate(['dashboard'])
    this.createComponent(HomePageComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
element.click();

  }
  goToVisit(link:any){ 
    this.title='My Visit History';
    this.createComponent(VisitHistoryComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
    element.click();

  }


  goToVital(link:any){ 
    this.title='Vital Signs';
    this.createComponent(VitalSignComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
element.click();

  }


  goToMedical(link:any){ 
    this.title='Medical History';
    this.createComponent(MedicalHistoryComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
element.click();

  }

  
  goLabResult(link:any){ 
    // this.router.navigate(['providers'])
    this.title='Laboratory Results';
   
    this.createComponent(LabresultComponent);
    this.isDashboard =false;
    this.active = link;
    let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
    element.click();

  }

  goToProfile(link:any){
  
    this.title='Patient Profile';
    this.createComponent(PatientProfileComponent);
    this.isDashboard =false;
    this.active = link;
//     let element: HTMLElement = document.getElementById('sidebar_model') as HTMLElement;
// element.click();

  }


  
  createComponent(componentToLoad:any){
    this.entry?this.entry.clear():null;  
    let component = this.componentFactoryResolver.resolveComponentFactory(componentToLoad);  
    let page1ComponentRef = this.entry.createComponent(component);  
    //( < Page1Component > page1ComponentRef.instance).title = 'Page1';  

  }



  logOut(){
    this.provider= localStorage.getItem('provider');
    this.provider=JSON.parse(this.provider);

    const msalConfig = {
      auth: {
          clientId: this.provider.clinet_id,
          authority: this.provider.athority,
          postLogoutRedirectUri:"http://localhost:8000"

      }
  };
  const msalInstance = new Msal.UserAgentApplication(msalConfig);


  msalInstance.logout();
  localStorage.clear();
  }
}
