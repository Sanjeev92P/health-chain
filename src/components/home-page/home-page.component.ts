import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  isDefault:boolean=true;
  isLoadedScreen:boolean=true;
  isInsurers:boolean=false;

  constructor() { }

  ngOnInit(): void {
  }


  	goToDetails(){
  		this.isLoadedScreen=false;
  	}
}
