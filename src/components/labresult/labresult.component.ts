import { Component, OnInit } from '@angular/core';

import { BaseService } from 'src/services/base.service';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-labresult',
  templateUrl: './labresult.component.html',
  styleUrls: ['./labresult.component.css']
})
export class LabresultComponent implements OnInit {
  container:any={};
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {


    this.apiService.getLabResult().subscribe((res:any)=>{
                  this.container = res.entry;

    },(err)=>{
  console.log("err ", err);
})  
  }



  getValue(obj:any,index:number,index2:number){
    if(obj){ 
      this.apiService.apiCall(environment.FHIRendpoint+'/'+obj).subscribe((res:any)=>{
        if(res.hasOwnProperty('valueQuantity')){
          let row = document.getElementById(`aa_value_${index}_${index2}`);
          let rows = document.getElementById(`aa_unit_${index}_${index2}`);
          if(row && rows){
            row.innerHTML = Math.round(res.valueQuantity.value).toString();
            rows.innerHTML = res.valueQuantity.unit;
          }
        }
      },(err)=>{
        console.log("err ", err);
      })
    } 
  }

}
