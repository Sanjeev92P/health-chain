import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/services/base.service';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isDefault:boolean=true;
  isLoadedScreen:boolean=true;
  container:any={};
  provider:any={};
  httpOptions: any;
  response: any;
  p: number = 1;
  config: any;
  constructor(private router:Router, public apiService : ApiService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    // this.provider= localStorage.getItem('provider');
    // this.provider=JSON.parse(this.provider);
    //localStorage.setItem('patient_id', this.provider.patient_id);
   // this.getPatient();
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: 0
    };
  }


  pageChanged(event:any){
    this.config.currentPage = event;
  }

  getPatient(){
    this.apiService.getPatientList().subscribe((res)=>{
      if(res){
        
        this.container = res; 
        }
     
      
    },(err)=>{
      console.log("My getPatient ====>>>..",err);
    });

  }



  choosePatient(value:any){
    
    localStorage.setItem('patient_id',value);
    this.router.navigateByUrl('/header');
  }


  
  login(username:any,password:any){

   let data= [{
      'patient_id':'a7cac9be-3f3c-44cd-86ea-956e6ecc6584',
      'username':'kertzmanncher'
    },{
      'patient_id':'7b770a15-10b7-45ab-a11c-3ccb4600caa6',
      'username':'marge'
    },{
      'patient_id':'b0ffcc6b-8e4b-4e6c-8e71-3d3908cb24af',
      'username':'earline'
    },{
      'patient_id':'510d812d-34a8-4a38-8744-3a6470c59e89',
      'username':'emma'
    },{
      'patient_id':'e4e69e9a-078e-4636-bd95-ecfe06c712b8',
      'username':'maria'
    } ,{
      'patient_id':'fc29e6ee-154f-4fad-9210-1eb6fd3c5e42',
      'username':'miller'
    }
  ];
    if(username){
    let user:any={};
    user=data.find((obj=>obj['username']==username));
    if(user){
        localStorage.setItem('patient_id',user.patient_id );
        this.router.navigateByUrl('header');
        }
    else{
      alert('Please enter a valid username');
    }
} else {
  alert('Please enter the username');
}
}

}
