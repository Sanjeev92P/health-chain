import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/services/base.service';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';



@Component({
  selector: 'app-medical-history',
  templateUrl: './medical-history.component.html',
  styleUrls: ['./medical-history.component.css']
})
export class MedicalHistoryComponent implements OnInit {

  isLoadedScreen:boolean=true;
  allergyintolerance:any={};
  condition:any={};
  immunization:any={};
  medications:any={};
  procedure:any={};
  container:any={};
  containers:any={};
  httpOptions: any;
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.isLoadedScreen=true;
    this.getMedicalhistory();
  }
  goToDetails(){
    this.isLoadedScreen=false;
  }
  back(){
    this.isLoadedScreen=true;
  }

  getMedicalhistory(){
    let provider:any=localStorage.getItem('provider');
    provider=JSON.parse(provider);
   // alert();
    this.apiService.getMedicalhistory().subscribe((res)=>{
      if(res){
          this.containers = res;
          this.container['userDetails'] =localStorage.getItem('userdetails');
          this.container['userDetails'] =JSON.parse(this.container['userDetails']);
        }
    },(err)=>{
      console.log("My getMedicalhistory ====>>>..",err);
    })

    this.apiService.apiCall(provider.endpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Procedure').subscribe((res)=>{
      if(res){
          this.procedure = res;
        }
    },(err)=>{
      console.log("My getMedicalhistory ====>>>..",err);
    });
    this.apiService.apiCall(provider.endpoint+'/Patient/'+localStorage.getItem('patient_id')+'/AllergyIntolerance').subscribe((res)=>{
      if(res){
          this.allergyintolerance = res;
        }
      },(err)=>{
      console.log("My getMedicalhistory ====>>>..",err);
    });

    this.apiService.apiCall(provider.endpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Immunization').subscribe((res)=>{
      if(res){
          this.immunization = res;
        }
    },(err)=>{
      console.log("My getMedicalhistory ====>>>..",err);
    });

    this.apiService.apiCall(provider.endpoint+'/Patient/'+localStorage.getItem('patient_id')+'/MedicationRequest').subscribe((res)=>{
      if(res){
          this.medications = res;
        }
    },(err)=>{
      console.log("My getMedicalhistory ====>>>..",err);
    });
  }


  getEncounterDetail(reference:any,key:any,value:number){

    this.apiService.apiCall(environment.FHIRendpoint+'/'+reference).subscribe((res:any)=>{
      if(res){
        let row= document.getElementById(key+value);
        if(row){
          row.innerHTML=res.type[0].text+'  ('+res.subject.display+')  '+res.status
        }
        }
       
    },(err)=>{
      console.log("My Encounter ====>>>..",err);
    });
  }


}
