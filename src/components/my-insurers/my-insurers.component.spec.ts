import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInsurersComponent } from './my-insurers.component';

describe('MyInsurersComponent', () => {
  let component: MyInsurersComponent;
  let fixture: ComponentFixture<MyInsurersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyInsurersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInsurersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
