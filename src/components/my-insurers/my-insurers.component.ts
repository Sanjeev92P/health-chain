import { BaseService } from 'src/services/base.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-my-insurers',
  templateUrl: './my-insurers.component.html',
  styleUrls: ['./my-insurers.component.css']
})
export class MyInsurersComponent implements OnInit {
  container:any={};
  containerr:any={};
  httpOptions: any;
  response: any;
  p: number = 1;
  config: any;
  isLoadedScreen:boolean=true;
  fdata:any=[];
  ffdata:any=[];
  pname:any;
  JSON:any;
  constructor(private apiService:ApiService) {
    this.JSON=JSON;
  //  this.getInsurance();
  }

  ngOnInit(): void {
    this.isLoadedScreen=true;
    this.getInsurance();
  }


  getInsurance(){
    this.apiService.getExplanationOfBenefit('').subscribe((res)=>{
      if(res){
          this.container = res;

          this.apiService.getCoverage('').subscribe((res:any)=>{
               this.container['coverage']=res;
           });
        }
    },(err)=>{
      console.log("My Organization ====>>>..",err);
    });

  }


  goToDetail(id:string,claim:any){
    this.isLoadedScreen=false;
  // alert(claim);
    this.apiService.getExplanationOfBenefit(id).subscribe((res:any)=>{
      if(res){
        this.apiService.apiCall(environment.FHIRendpoint+'/'+res.patient.reference).subscribe((res:any)=>{
          this.container['patient_name']=res.name?res.name[0].family+', '+res.name[0].given.join(' ') :''
        }
        )
        console.log('exp',res);
        //this.container['patient_name']=
        this.container['ExplanationDetails'] = res;
        this.apiService.getClaim(res.claim.reference).subscribe((res:any)=>{
            this.container['claim_sequence'] = res.insurance[0].sequence;
            this.container['claim_provider'] = res.provider.display;
          });
          this.apiService.getEncounter(res.id).subscribe((res:any)=>{
            this.container['encounter']=res;  
          });




       }
   },(err)=>{
     console.log("My Organization ====>>>..",err);
   });

    }
	
	


  getOrgNameBasedonID(obj:any,index:number){
    if(obj){
      this.apiService.getOrganizationName(obj).subscribe((res:any)=>{
        if(res.hasOwnProperty('name')){
          let row = document.getElementById(`org_${index}`)
          if(row){
            row.innerHTML = res.name;

            return res.name;
          }
        } 
      },(err)=>{
        console.log("err ", err);
      })
    }
  }


  getClaimBasedonID(obj:any,index:number){
    if(obj){
      this.apiService.getClaim(obj).subscribe((res:any)=>{
        //if(res.hasOwnProperty('name')){
          let row = document.getElementById(`claim_${index}`);
          let provider = document.getElementById(`org_${index}`);
          if(row && provider){
            row.innerHTML = res.insurance[0].sequence;
            provider.innerHTML=res.provider.display;
           // return res.provider.display;
          }
       // }
      },(err)=>{
        console.log("err ", err);
      })
    }
  }


   getPatientName(obj:any,index:number){
    if(obj){
      this.apiService.getPatientData(obj).subscribe((res:any)=>{
        if(res.hasOwnProperty('name')){
          let row = document.getElementById(`patient_${index}`)
          if(row){
            row.innerHTML = res.name[0].family+ ' '+res.name[0].given[0];
          }
        }
      },(err)=>{
        console.log("err ", err);
      })
    }
  }



}
