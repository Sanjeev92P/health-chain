import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/services/base.service';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';


@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.component.html',
  styleUrls: ['./patient-profile.component.css']
})
export class PatientProfileComponent implements OnInit {
  isLoadedScreen:boolean=true;
  container:any={};
  httpOptions: any;
  Observation:any={};
  phone:any;
  email:any;
  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.isLoadedScreen=true;
    this.getPatient();
  }
  goToDetails(){
    this.isLoadedScreen=false;
  }
  back(){
    this.isLoadedScreen=true;
  }

  getPatient(){
    this.apiService.getPatientData('').subscribe((res:any)=>{
      if(res){
          this.container = res;
          let data:[] =res.telecom;
          this.phone = data.find((obj=>obj['system']=='phone'));
          this.email =data.find((obj=>obj['system']=='email'));

          this.container['userDetails'] =localStorage.getItem('userdetails');
          this.container['userDetails'] =JSON.parse(this.container['userDetails']);
        }
    },(err)=>{
      console.log("My getPatient ====>>>..",err);
    });


    this.apiService.getGoal('').subscribe((res)=>{
      if(res){
          this.container['CarePlan'] = res;

          console.log("container['CarePlan']",this.container['CarePlan'])
        }
    },(err)=>{
   //   console.log("My Organization ====>>>..",err);

  });


  this.apiService.Observation('').subscribe((res:any)=>{
    if(res){
        this.Observation = res;

      }
  },(err)=>{
  console.log("My Organization ====>>>..",err);

});


  }


}
