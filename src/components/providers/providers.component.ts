import { BaseService } from 'src/services/base.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';
@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.css']
})
export class ProvidersComponent implements OnInit {
  isLoadedScreen:boolean=true;
  container:any={};
  containers:any=[];
  encounter:any=[];
  JSON:any;
  constructor(private apiService:ApiService) {
    this.JSON = JSON;
   }

  ngOnInit(): void {
    this.isLoadedScreen=true;
    this.getPrecticinor();
   // this.getProviders();
  }
  goToDetails(){
    this.isLoadedScreen=false;
  }

  back(){
    this.isLoadedScreen=true;
  }

  getPrecticinor(){
    this.apiService.getEncounter('').subscribe((res:any)=>{
      if(res){
        console.log('res',res);
        res.entry.forEach((element:any) => {
          element.resource.participant.forEach((el:any) => {
            this.encounter.push(el.individual.reference);
          });
       });

      }
      this.getProviders();
      console.log("My getPrecticinor",this.encounter);
    },(err)=>{
      console.log("My getPatient ====>>>..",err);
    });

  }

  getProviders() {

    this.container['provider'] =localStorage.getItem('provider');
    this.container['provider'] =JSON.parse(this.container['provider']);

    this.encounter = this.encounter.filter((n:any, i:any) => this.encounter.indexOf(n) === i);

    for (var val of this.encounter) {
          this.apiService.apiCall(environment.FHIRendpoint+'/'+val).subscribe((res)=>{
            if(res){
                this.containers.push(res);
              }
          },(err)=>{
          })
    }
    this.apiService.getCarePlan('').subscribe((res)=>{
      if(res){
          this.container['CarePlan'] = res;
        }
    },(err)=>{
    });
  }




}
