import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/services/base.service';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { runInThisContext } from 'vm';
@Component({
  selector: 'app-visit-history',
  templateUrl: './visit-history.component.html',
  styleUrls: ['./visit-history.component.css']
})
export class VisitHistoryComponent implements OnInit {
  isDefault:boolean=true;
  isLoadedScreen:boolean=true;
  container:any={};
  httpOptions: any;
  response: any;
  p: number = 1;
  config: any;

  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
    this.isDefault=true;
    this.getVisit();
    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: 0
    };
  }
  goToDetails(){
    this.isLoadedScreen=false;
  }
  pageChanged(event:any){
    this.config.currentPage = event;
  }
  back(){
    this.isLoadedScreen=true;
  }

  getVisit(){
    this.apiService.getEncounter('').subscribe((res:any)=>{
      if(res){
          this.container = res;
          this.config.totalItems = res['entry']?res['entry'].length:0;
          this.container['provider'] =localStorage.getItem('provider');
          this.container['userDetails'] =localStorage.getItem('userdetails');
          this.container['userDetails'] =JSON.parse(this.container['userDetails']);
          this.container['provider'] =JSON.parse(this.container['provider']);
        }
    },(err)=>{
      console.log("My getPatient ====>>>..",err);
    })
  }




  goToDetail(url:string){
    this.isLoadedScreen=true;
    this.isDefault=false;
    this.apiService.apiCall(url).subscribe((res:any)=>{
      if(res){
          this.container['details'] = res;
          this.apiService.getOrganizationName(res.serviceProvider.reference).subscribe((res:any)=>{
                this.container['provider_name'] = res;
          },(err)=>{
            console.log("err ", err);
          })
          this.apiService.getPatientData(res).subscribe((res:any)=>{

            this.container['patient_name'] = res.name;
            //this.getClinicalDetails();
      },(err)=>{
        console.log("err ", err);
      })
        }
        console.log("My getPatient",this.container);
    },(err)=>{
        console.log("My getPatient ====>>>..",err);
    })

    this.apiService.getLabResult().subscribe((res:any)=>{
    this.container['LabResult'] = res.entry;
    },(err)=>{
  console.log("err ", err);
});
}
  getClinicalDetails(){
    this.apiService.getClinincalReports().subscribe((res:any)=>{
      if(res.hasOwnProperty('entry') && res.entry.length>0){
        this.container['clinicalReports'] = res.entry;
      }
    }),(err:any)=>{
      console.log("Errr",err)
    }
  }
  getLaboratoryResults(){

  }

  getOrgNameBasedonID(obj:any,index:number){
    if(obj){
      this.apiService.getOrganizationName(obj.reference).subscribe((res:any)=>{
        if(res.hasOwnProperty('name')){
          let row = document.getElementById(`org_${index}`)
          if(row){
            row.innerHTML ="<i class='fa fa-hospital-o' aria-hidden='true' style='margin-right:8px;'></i>" +res.name;
          }
        }
      },(err)=>{
        console.log("err ", err);
      })
    }
  }


  getOrgAdrBasedonID(obj:any,index:number){
    if(obj){
      this.apiService.getOrganizationName(obj.reference).subscribe((res:any)=>{
        if(res.hasOwnProperty('name')){
          let row = document.getElementById(`orgadr_${index}`)
          if(row){
            row.innerHTML = "<i class='fa fa-envelope' aria-hidden='true' style='margin-right:8px;'></i>"+ res.address[0].line[0]+" "+res.address[0].city+" "+res.address[0].state;
          }
        }
      },(err)=>{
        console.log("err ", err);
      })
    }
  }

  getPatientData(obj:any,name:any){

    if(obj){
      this.apiService.getPatientData(obj).subscribe((res)=>{
        console.log("Org Name ", res);
      },(err)=>{
        console.log("err ", err);
      })
    }

  }






}
