import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from 'src/services/api.service';
import { BaseService } from 'src/services/base.service';
import { runInThisContext } from 'vm';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-vital-sign',
  templateUrl: './vital-sign.component.html',
  styleUrls: ['./vital-sign.component.css']
})
export class VitalSignComponent implements OnInit {
  container:any={};
  datePipeEn: DatePipe = new DatePipe('en-US')

  constructor(public apiService:ApiService, private baseService:BaseService,public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.getVitals();
    this.getVitalsConfig();
  }
  getVitals(){
    this.apiService.getVitals().subscribe((res:any)=>{
      if(res && res['entry'] && res.entry.length>0){
        this.container['main_data'] = res; 
      }
    },(err)=>{
      console.log("Err",err)
    })
  }
  getVitalsConfig(){
   this.container['config'] =  this.baseService.getVitalsConfig();
  }
  getValue(_code:any){
    if(_code && this.container.main_data && this.container.main_data.hasOwnProperty('entry')){
      if(this.container.main_data.entry.length>0){
        let val = this.container.main_data.entry.find((x:any)=>{
              return x.resource.code.coding[0].code == _code
      });

        val =val?val.resource:{};
        if(val && val.hasOwnProperty('valueQuantity')){
          let value = val.valueQuantity.value.toFixed(2);
          return  value.concat(' ',val.valueQuantity.code).concat(' ',this.datepipe.transform(val.effectiveDateTime, 'MM/dd/YYYY'))          ;
        }
      }
    }
  }
}
