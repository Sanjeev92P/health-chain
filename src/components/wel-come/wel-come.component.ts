import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http'; 
import FHIR, { client } from "fhirclient";
import * as Msal from "msal";
import { ApiService } from 'src/services/api.service';
import { environment } from 'src/environments/environment';

declare var require: any;



@Component({
  selector: 'app-wel-come',
  templateUrl: './wel-come.component.html',
  styleUrls: ['./wel-come.component.css']
})
export class WelComeComponent implements OnInit {
  posts:any;
  container:any={};
  provider:any={};
  selected:boolean=false;
  code:string='';
  state:string='';
  myApp:any={};
  ab:any={};
  constructor(private router:Router, public common : ApiService,private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.common.apiCall(environment.json_url).subscribe((res:any)=>{
      this.container['providers'] =res;
     });
    console.log("ready function");
    console.log(Router.toString);
    this.route.queryParams.subscribe(params => {
      this.code = params['code'];
      this.state = params['state'];
      console.log(this.code);// OUTPUT 1534
      console.log(this.state);// OUTPUT red
    });
  }
  goToLogin(){
    this.common.login().subscribe((res:any)=>{
    });
  if(this.selected){


    this.provider= localStorage.getItem('provider');
    this.provider=JSON.parse(this.provider);
    //this.common.loginApi(this.provider.client_id,this.provider.client_secret,this.provider.authority);

  //  alert(this.provider.endpoint);
    if(this.provider.name=='Cerner' || this.provider.name=='United Health'){
     // alert('hdgshs');
      FHIR.oauth2.authorize({
      "client_id": this.provider.client_id,
      "scope": this.provider.scope,
      "redirect_uri":this.provider.redirect_uri,
      "iss":this.provider?this.provider.endpoint:'',
      "fhirServiceUrl" : this.provider?this.provider.endpoint:'',
  })
    }else{
      
      // FHIR.oauth2.authorize({
      //   "client_id": "0c5beded-a782-4485-beb6-f5555314bd97",
      //   "scope": "patient/Coverage.read patient/ExplanationOfBenefit.read patient/Patient.read patient/Procedure.read",
      //   "redirect_uri":"https://188.166.226.241/",
      //   "iss":'https://sandbox.fhir.flex.optum.com/R4',
      //   "fhirServiceUrl" : 'https://sandbox.authz.flex.optum.com',
      //    });
      this.common.login().subscribe((res:any)=>{
      });
      //this.goToServices();
  }
}
  else{

    
    alert('Please Choose a Provider');
  }
}

  getProvider(value:any){
    if(value!='Choose a health care provider'){
    let data:[] =this.container['providers'];
    //console.log('ffgfgfgfg',data);
      this.provider =data.find((obj=>obj['name']==value))
      this.provider=JSON.stringify(this.provider);
      localStorage.setItem('provider',this.provider);
      this.selected=true;
    }
  }



  goToServices(){
    const msalConfig = {
      auth: {
          clientId: this.provider.client_id,
          authority: this.provider.authority,
      }
  };

  const msalInstance = new Msal.UserAgentApplication(msalConfig);



  var loginRequest = {
    scopes: [this.provider.scope] // optional Array<string>
};

 msalInstance.loginPopup(loginRequest)
     .then(response => {
      if (msalInstance.getAccount()) {
        var tokenRequest = {
            scopes: [this.provider.scope]
        };
        msalInstance.acquireTokenSilent(tokenRequest)
            .then(response => {
              console.log('accessToken : '+response.accessToken);
              localStorage.setItem('token',response.accessToken);
              this.router.navigateByUrl('login');
            })
    } else {
        // user is not logged in, you will need to log them in to acquire a token
    }
     })
     .catch(err => {
         // handle error
     });

    // this.ab= localStorage.getItem('token');
    // if(this.ab){
    //    this.router.navigateByUrl('header');
    //  }
   }


}
