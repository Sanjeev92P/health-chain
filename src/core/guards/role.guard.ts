import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanLoad, Router, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Route } from '@angular/compiler/src/core';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanLoad {

  constructor(private auth: AuthService, private router: Router) {}
  
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    
    let path = segments[0].path; 
    if (this.auth.isAuthenticated()) {
    }

    this.router.navigate(['/login']);
    return false;
  }
  
}
