import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpResponse} from '@angular/common/http';
import {HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,private router:Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.getToken()) {
      const authReq = req.clone({
        // headers: req.headers.set(
        //   'Authorization',  `Bearer ${this.authService.getToken()}`,
        // ),
        
      });
      req = authReq;
    }
    return next.handle(req).pipe(
      tap(
        event => this.handleResponse(req, event),
        error => this.handleError(req, error)
      )
    );
  }


  handleResponse(req: HttpRequest<any>, event:any) {
    if (event instanceof HttpResponse) {
    }
  }

  handleError(req: HttpRequest<any>, event:any) {
    if (event instanceof HttpErrorResponse) {
      if (event.status === 401) {
        localStorage.clear();
        this.router.navigateByUrl('/');
      }
    }
    // console.error('Request for ', req.url,
    //       ' Response Status ', event.status,
    //       ' With error ', event.error);
  }
}