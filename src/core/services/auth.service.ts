import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  getUserDetails() {
    let LOGIN_INFO = localStorage.getItem('LOGIN_INFO');
    return LOGIN_INFO && JSON.parse(LOGIN_INFO);
  }
  isAuthenticated(): boolean {
    let LOGIN_INFO = this.getUserDetails();
    if (LOGIN_INFO && LOGIN_INFO.EMAIL) {
      return true;
    } else {
      return false;
    }
  }

  isUser(): boolean {
    return true;
  }

  getToken() {
    return localStorage.getItem('token')?localStorage.getItem('token'):'';
  }

  userRole() {
    return 'user'
  }

}
