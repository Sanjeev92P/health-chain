import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BOOL_TYPE } from '@angular/compiler/src/output/output_ast';
import { url } from 'inspector';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  httpOptions: any;
  constructor(private _http:HttpClient) { 
  }
  getVitals(){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Observation';
    return this._http.get(path) 
  }


  getProvider(id:any){
    let path = environment.FHIRendpoint+'/Organization';
    if(id){
      path= path+'/'+id;
    }
    return this._http.get(path) 
  }

  getPatient(){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id');
    return this._http.get(path) 
  }

  getPatientList(){
    let path = environment.FHIRendpoint+'/Patient?_count=50';
    return this._http.get(path) 
  }

  getCoverage(id:any){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Coverage';
    // if(id){
    //   path= path+'/'+id;
    // }
    // else{
     // path=path+'/4f11ae4a-d18f-4eb6-8ab4-72a5261a827b';
   // }
    return this._http.get(path);
  }

  apiCall(path:string){
    return this._http.get(path);
  }

  
  apiPost(name:string,client_id:string,client_secret:string, authority:string,redirect_uri:string,scope:string,endpoint:string,bypasslogin:string,patient_id:string,update:string){
         const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      };

      const formData = new FormData();
      formData.append('name', name);
      formData.append('client_id', client_id);
      formData.append('client_secret',client_secret);
      formData.append('authority', authority);
      formData.append('redirect_uri', redirect_uri);
      formData.append('scope', scope);
      formData.append('endpoint', endpoint);
      formData.append('bypasslogin',bypasslogin);
      formData.append('patient_id', patient_id);
      formData.append('update', update);


      return this._http.post<any>(`${environment.json_url}`,formData);
   // return this._http.post(environment.json_url,body,httpOptions);
  }

  
  getExplanationOfBenefit(id:string){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/ExplanationOfBenefit';
    if(id){
      path= environment.FHIRendpoint+'/ExplanationOfBenefit/'+id;
    }
    return this._http.get(path) 
  }

  
  getClaim(id:string){
    let path = environment.FHIRendpoint+'/Claim/';
    //let path = environment.FHIRendpoint+'/Claim';
    if(id){
      path= environment.FHIRendpoint+'/'+id;
    }
    return this._http.get(path) 
  }



  getCarePlan(id:string){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/CarePlan';
    // if(id){
    //   path= path+'/'+id;
    // }
    return this._http.get(path) 
  }


  getGoal(id:string){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Goal';
    // if(id){
    //   path= path+'/'+id;
    // }
    return this._http.get(path) 
  }



  getEncounter(id:any){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Encounter';
    //  if(id){
    //    path= path+'/'+id;
    //  }else{
    //   path=path+'/4f11ae4a-d18f-4eb6-8ab4-72a5261a827b';
    // }
    return this._http.get(path) 
  }
  getClinincalReports(){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/DiagnosticReport';
    return this._http.get(path) 
  }
  getMedicalhistory(){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Condition';
    return this._http.get(path) 
  }
  getOrganizationName(orgID:string){ 
    let path = environment.FHIRendpoint+'/'+orgID;
    return this._http.get(path) 
  }

  getPatientData(obj:string){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id');
    return this._http.get(path) 

  }


  getLabResult(){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/DiagnosticReport';
    return this._http.get(path) 

  }


  Observation(obj:string){
    let path = environment.FHIRendpoint+'/Patient/'+localStorage.getItem('patient_id')+'/Observation?code=72166-2';
    return this._http.get(path) 

  }

login(){
const httpOptions = {
   headers: new HttpHeaders(
    {'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'}
   )
 };
 var CLIENT_ID='0c5beded-a782-4485-beb6-f5555314bd97';
 var BASE_URL="https://sandbox.authz.flex.optum.com";
 var OPAQUE_STATE='';
 var AUTHORIZE_REDIRECT_URI="https://188.166.226.241";
 var SCOPE="patient/Coverage.read patient/ExplanationOfBenefit.read patient/Patient.read patient/Procedure.read";
 var CODE_CHALLENGE='fce7af7e001d516eb249e95d8a9d222152ddb7a261ed0f8f45ce073111081fb9'
 var CODE_VERIFIER=''

 var uri='response_type=code&state=&client_id=0c5beded-a782-4485-beb6-f5555314bd97&scope=patient%2FCoverage.read%20patient%2FExplanationOfBenefit.read%20patient%2FPatient.read%20patient%2FProcedure.read&redirect_uri=https%3A%2F%2F188.166.226.241%2F&code_challenge=IFwUUdlbD4WFnFJlTl7kn6fOKJIB2OyV-KiPXEKuU1E&code_challenge_method=S256';
 return this._http.get<any>(BASE_URL+'/oauth/authorize?'+uri,httpOptions);
}

}
