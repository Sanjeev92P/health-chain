import { Injectable, Type, ComponentFactoryResolver } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpParams, HttpParameterCodec } from '@angular/common/http';
import { map, catchError, flatMap } from 'rxjs/operators';
import { empty, throwError, Observable } from 'rxjs';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { AuthService } from './auth.service';
import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BaseService {
  baseUrl = ""//environment.baseUrl;
  headers: any = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/fhir+xml;q=1.0, application/fhir+json;q=1.0, application/xml+fhir;q=0.9, application/json+fhir;q=0.9'
  };
  constructor(
    private _http: HttpClient,
    public router: Router,
    private titleService: Title,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private sanitizer: DomSanitizer,
    private apiService:ApiService
    
  ) { 
    
  }
  getClaims(body:any) {
    let path = '/Claim';
    let url = this.baseUrl + path; 
    return this._http.get(url,{headers:this.headers});
  }
  getEncounter(body:any) {
    let path = '/Encounter';
    let url = this.baseUrl + path; 
    return this._http.get(url,{headers:this.headers});
  }
  getImmunization(body:any) {
    let path = '/Immunization';
    let url = this.baseUrl + path; 
    return this._http.get(url,{headers:this.headers});
  }
  getOrganization(body:any) {
    let path = '/Organization';
    let url = this.baseUrl + path; 
    return this._http.get(url,{headers:this.headers});
  }
  getToken(){
    let path = '/Organization';
    let url = this.baseUrl + path; 
    return this._http.get(url,{headers:this.headers});
  }
  getVitalsConfig(){
    let config = [
     
      {
        text:'Blood Pressure',
        code:'85354-9'
        },{text:'Diastolic Blood Pressure',
        code:'8462-4'
        },{text:' Systolic Blood Pressure',
        code:'8480-6'
        },{text:'Body weight',
        code:'29463-7'
        },{text:'Body Height',
        code:'8302-2'
        },{text:'Heart Rate',
        code:'8867-4'
        },{text:'Pulse Oximetry',
        code:['2708-6','59408-5','150456']
        },
        {text:'Inhaled Oxygen Concentration BMI Percentile (2 - 20 years): ',code:''},
        {text:'Respiratory Rate',
        code:'9279-1'
        },{text:'Body Temperature',
        code:'8310-5'
        },{text:'Weight-for-length Percentile',
        code:'' 
        },{text:'Head Occipital-frontal Circumference',
        code:'9843-4'
        },{
          text:'Body mass index (BMI)',
          code:'39156-5'
        },{
          text:'Weight-for-length Percentile',
          code:'39156-5'
        }

        
    ]
    return config
  }
  
}
